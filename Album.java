public class Album
{
	public String name;
	public String artist;
	public double price;
	public int releaseYear;
	
	public Album()
	{
		this.name = "Default";
		this.artist = "Default";
	}
	
	public boolean checkDiscount(String artist)
	{
		boolean discount = false;
		
		if (this.artist.equals("DragonForce"))
		{
			discount = true;
		}
		
		return (discount);
	}
}