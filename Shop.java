import java.util.Scanner;

public class Shop
{
	public static void main(String[] args)
	{
		Scanner reader = new Scanner(System.in);
		Album[] albums = new Album[4];
		
		//User inputs the info on the album they want to input
		for (int i = 0; i < 4; i++)
		{
			System.out.println("Current Album: Album #"+(i+1));
			albums[i] = new Album();
			
			System.out.println("Enter The Name Of The Album");
			albums[i].name = reader.nextLine();
			System.out.println("Enter The Artist Of The Album");
			albums[i].artist = reader.nextLine();
			System.out.println("Enter The Price Of The Album");
			albums[i].price = Double.parseDouble(reader.nextLine());
			System.out.println("Enter The Release Year Of The Album");
			albums[i].releaseYear = Integer.parseInt(reader.nextLine());
		}
		
		//Printing the info the user gave us on the last album
		System.out.println("The Name Of The Last Album You Entered is: "+albums[3].name);
		System.out.println("The Artist Of The Last Album You Entered is: "+albums[3].artist);
		System.out.println("The Price Of This Album is: "+albums[3].price);
		System.out.println("The Release Year Of This Album is: "+albums[3].releaseYear);
		
		//Checking if there is a discount on the last album using the checkDiscount() method
		if (albums[3].checkDiscount(albums[3].artist))
		{
			System.out.println("You're a DragonForce fan too?! Have a $5.00 discount!");
		}
	}
}